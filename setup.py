

from setuptools import setup, find_packages
from os import path

# Get the long description from the README file
with open(path.join(path.abspath(path.dirname(__file__)),'README.txt')) as f:
    long_description = f.read()

setup(
    name='echolab',
    version='2.0.2',
    author='Zachary Berkowitz',
    author_email='zac.berkowitz@gmail.com',
    maintainer='Rick Towler',
    maintainer_email='rick.towler@noaa.gov',
    description='SIMRAD acoustic data processing library',
    long_description=long_description,
    license="BSD",
    packages=find_packages(),
    install_requires=['numpy >= 1.7.0',
                      'pandas == 0.19.2',
                      'h5py >= 2.0.0',
                      'pytz',
                      'scipy'],
    include_package_data=False,
    zip_safe=True,
    classifiers=['Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 2.7',
        'Natural Language :: English',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities']
)
