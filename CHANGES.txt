This file contains change information about the latest release of pyEcholab.

pyEcholab 2.0.2 (1/27/16)

  - Changed the default value of the easterly_positive keyword of
    RawReader.interpolate_ping_gps() to True so by default conventional
    lat/lons are returned where values east of 0 lat are positive and
    values west are negative.
  
  - Various fixes to enable package to work with Pandas 0.19.2, numpy 1.11.3,
    and matplotlib 2.0.0.

pyEcholab 2.0.1 (9/30/14)

  - Fixed bug where the alongship and athwartship angles were swapped. When
    converting to indexed angles from the raw 16 bit data, the lower 8 bits
    were stored as the alongship, and the upper 8 bits were stored as athwart.
    This was swapped, so the lower 8 bits now are stored in the athwartship
    series and the upper in the alongship series.
    
  - Numerous changes were made to accommodate internal changes in Pandas. It
    seems that between versions 0.11.0 and 0.14.1 significant changes were
    made to Pandas indexing which broke various methods in raw_reader.py. It
    is recommended that you only use Pandas 0.14.1 with this version of
    pyEcholab and that you only upgrade Pandas when pyEcholab has been verified
    to work with the new version.
    
  - Added new examples and a simple test script. Added reference data files
    to be used for testing pyEcholab after making internal changes.
    
  - Added "easterly_positive" keyword to raw_reader.interpolate_ping_gps().
    When set to True, interpolated GPS values will have positive easterly values
    and negative westerly values. Default value is False. 