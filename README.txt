echolab is a python 2.7.x library for analyzing acoustic data produced
by SIMRAD EK60, ES60 and ME70 echosounders.  

File IO:
    Reads/writes raw-data files (.raw, .bot, .out) for EK60 and ES60 echosounders.
    Support for ME70 files is experimental.

Calibration:
    Import/export EchoView .ecs calibration files.

Griding:
    Arbitrary referenced layers with 'surface', 'bottom', and 'transducer face' presets

Analysis:
    Single-beam/Split-beam mean Sv integrations

Masking:
    Sample-based masking by threshold
    Range-based masking (exclude above/below/between/outside range-referenced lines)
    Ping-based masking (exclude whole pings based on some function)

Misc:
    Remove triangle-wave offset present in SIMRAD ES60 data
    Check for and fix incorrect clock settings on original acquisition computer
    Interpolate/smooth GPS, vessel distance, and bottom depth values per-ping

Requirements:

Echolab depends heavily on and works only with certain versions of Pandas,
the Python data analysis package. It is strongly recommended that you use
pandas 0.19.2.

The Echolab developers use WinPython 2.7.10.3 with pandas 0.19.2, numpy 1.11.3,
scipy 0.18.1, h5py 2.5.0, numexpr 2.6.1, and pytz 2015.6. From a vanilla
WinPython 2.7.10.3 install you must upgrade numpy, pandas, numexpr, and scipy.
Note that 2.7.10.3 is the last full non-beta WinPython release based on Python 2.7.

WinPython 2.7.10.3 can be downloaded from:
    https://sourceforge.net/projects/winpython/files/WinPython_2.7/2.7.10.3/

The WinPython homepage:
    https://winpython.github.io/
    
Windows users can get the latest versions of Pandas, Numpy, Scipy and a load
of other great packages in "wheel" form from Christoph Gohlke's excellent
archive of unofficial windows binaries for Python packages.
    http://www.lfd.uci.edu/~gohlke/pythonlibs/

Also note that versions of matplotlib >= 1.5.0rc3 have issues plotting data
in pandas objects where an axes is comprised of datetime64 objects. See the
surface/bottom integration examples where the bottom reference/surface reference
lines are plotted for an example of a workaround. This issue still exists in
matplotlib 2.0.0.
	
pyecholab was developed under contract by Zac Berkowitz for the National Oceanic and
Atmospheric Administration (NOAA). Further development and maintenance by Rick Towler
(rick.towler@noaa.gov)