# -*- coding: utf-8 -*-
"""
@author: rick.towler

determine_time_offset is a simple script that will attempt to extract
GPS time and compare it to the PC time.

Often when working with data from vessels of opportunity the logging
PC's clock and time zone are not set correctly and so the "ping times"
stored in the .raw file are incorrect. The RawReader class has a
couple of methods to identify and correct for these mishaps and this
example plays around with the data a bit to show the offset and then
calls the check_timestamps method to correct them.

"""

import pytz
import echolab
import logging
import datetime

#  set up logging - used to give feedback from the ecolab package
logging.basicConfig(level=logging.DEBUG)

#  define the paths to a data file
raw_filename = 'C:/temp/L0505-D20060117-T145139-ES60.raw'
raw_filename = 'E:/DY1700/Data/EK60/Raw/DY1700_EK60-D20170124-T053940.raw'

#  read in the .raw data file. This returns a "RawReader" object that contains the data.
raw_data = echolab._io.raw_reader.RawReader([raw_filename])

#  extract the available NMEA data types. This will return a Pandas series object
#  that contains columns for the sentence type, talker ID and number of datagrams
#  for each talker/sentence. Note that there may be errant talker and sentence
#  entries that are a result of serial transmission errors. For example, you could
#  have a few sentence types of "CLL", "ELL", and "FLL" which are GLL sentences
#  that were munged along the way from the GPS to the recording PC.
nmea_types = raw_data.get_nmea_types()

#  we're first going to try to get the GPS date and time from the RMC sentence. The
#  RMC sentence is the only common GPS sentence with both date and time
if ('RMC' in nmea_types):
    #  get the RMC NMEA data - returns pandas dataframe indexed by Simrad datagram
    #  time. Simrad datagram time is the logging PC's clock time, converted to
    #  GMT using the PC's configured time zone.
    rmc_df = raw_data.get_nmea('RMC')

    #  convert the first GPS date and time value into strings
    gps_date = '{0:06d}'.format(int(rmc_df.date[0]))
    gps_time = '{0:06d}'.format(int(rmc_df.UTC[0]))

    #  now create a datetime object
    gps_datetime = datetime.datetime.strptime(gps_date + '-' + gps_time, '%d%m%y-%H%M%S')

    #  extract the PC's datetime
    pc_datetime = rmc_df.UTC.index[0]


#  No RMC, so we'll try GGA. GGA doesn't have a date component so you have
#  to make some assumptions which may or may not work out. You could do this
#  with GLL too, but this is just an example....
elif ('GGA' in nmea_types):

    #  get the GGA NMEA data
    gga_df = raw_data.get_nmea('GGA', ignore_checksum=True)

    #  convert the first GPS time value into a datetime time object
    gps_time = '{0:06d}'.format(int(gga_df.UTC[0]))
    gps_time = datetime.datetime.strptime(gps_time, '%H%M%S').time()

    #  get the PC's date and hour - in our gga dataframe this is stored in the
    #  index since the dataframe is indexed by the raw NMEA datagram timestamp.
    #  IOW, it is indexed by the PC time converted to UTC at the time the NMEA
    #  datagram was received by the logging PC.
    pc_hour = gga_df.UTC.index[0].hour
    pc_datetime = gga_df.UTC.index[0]
    pc_date = pc_datetime.date()

    #  now determine if we need to add or subtract a day from the date
    if (pc_hour <= 12) & (gps_time.hour > (pc_hour + 12)):
        #  it appears that the GPS is behind and hasn't rolled over into
        #  a new day so we need to subtract a day to get our GPS date
        gps_date = pc_date - datetime.timedelta(days=1)

    elif (pc_hour > 12) & (gps_time.hour < (pc_hour - 12)):
        #  it appears that the GPS is ahead and into the next day
        #  so we need to add a day to get our GPS date
        gps_date = pc_date + datetime.timedelta(days=1)

    else:
        #  it seems that the GPS and PC date are the same
         gps_date = pc_date

    #  now create the gps datetime object
    gps_datetime = datetime.datetime.combine(gps_date, gps_time)


#  set the timezone of the GPS datetime (have to do this, since you can't
#  subtract timezone aware and timezone naive datetimes)
gps_datetime = pytz.utc.localize(gps_datetime)

#  display the GPS time and the PC time.
print('')
print('')
print('Source .raw file:' + raw_filename)
print('')
print('Computer time: ' + pc_datetime.strftime("%Y-%m-%d %H:%M:%S"))
print('     GPS time: ' + gps_datetime.strftime("%Y-%m-%d %H:%M:%S"))

#  calculate the time offset.
time_offset = gps_datetime - pc_datetime
print('  time offset: ' + str(time_offset.total_seconds()))


#  everything we did above (and more) is already handled in the check_timestamps
#  method so if you think you might need to correct your data for a time offset
#  you can simply call the check_timestaps method. By default, the check_timestamps
#  method will return a dict keyed by the raw file name that contains the
#  min, max, and mean time offset in seconds.
#
#  With some NMEA sources you may need to set ignore_checksum to True since
#  some sources don't seem to calculate valid checksums and all of the NMEA
#  data ends up being dropped.
timeCheck = raw_data.check_timestamps(ignore_checksum=True)

#  you can then call adjust_timestamps to do the actual correction to your data.
#  this is done in memory and no changes are made to the data file. We only
#  correct the data if the time offset is greater than 1 second.
if (timeCheck.values()[0]['mean_offset'] > 1.0):
    raw_data.adjust_timestamps(timeCheck)

    #  or you would do the whole deal in one call to check_timestamps by setting the
    #  adjust_timestamps keyword to True which will cause check_timestamps to call
    #  adjust_timestamps automagically.
    #  timeCheck = raw_data.check_timestamps(adjust_timestamps=True)

    #  get the updated NMEA GGA data so we can confirm the correction
    gga_df = raw_data.get_nmea('GGA', ignore_checksum=True)

    #  extract the PC time from it
    pc_datetime = gga_df.UTC.index[0]

    #  and display the corrected time
    print('Corrected computer time: ' + pc_datetime.strftime("%Y-%m-%d %H:%M:%S"))
else:
    #  clock is off less than 1 second, do not adjust
    print('Logging PC time is < 1 second off the GPS time. No adjustment made.')









