# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 20:42:23 2014

@author: rtowler

"""

import echolab
from echolab import plotting as echoplot
import numpy as np
from matplotlib import pyplot as plt
import logging


#  set up logging - used to give feedback from the ecolab package
logging.basicConfig(level=logging.DEBUG)

#  define the alpha value assigned to the masked data when plotting
#  0 = transparent (can't tell what was masked) and 255 is opaque (can't
#  see the sample data underneath the mask). This is only for display and has
#  no effect on integration.
mask_alpha = 100


#  specify the interval length in nmi and cell height in m. This defines the basic
#  integration grid parameters
interval_length_nmi = 0.2
cell_height_m = 10


#  define the paths to a data and bottom file - we need the bottom file since
#  we will be creating a bottom referenced exclusion line and as of now
#  echolab doesn't have a bottom detection method. pyEcholab will read both
#  .out and .bot bottom files.
raw_filename = 'C:/test_data/L0208-D20140705-T122904-ES60.raw'
bot_filename = 'C:/test_data/L0208-D20140705-T122904-ES60.out'


#  define the output file names. We will write the integration output into csv files
integration_out_csv = 'C:/test_data/surface_ref_example_results_out.csv'
mask_out_csv = 'C:/test_data/surface_ref_example_mask_out.csv'


#  read in the .raw data file and the bottom file - this returns a "RawReader"
#  object that contains the data.
raw_data = echolab._io.raw_reader.RawReader([raw_filename, bot_filename])

#  calculate inter-ping distances using GPS data or VLW NMEA datagrams
raw_data.interpolate_ping_dist(ignore_checksum=True)

#  interpolate GPS data - required if you want start/end interval GPS data in the
#  integration esults output.
raw_data.interpolate_ping_gps(ignore_checksum=True)

#  calculate the bottom range for each ping using the bottom data in the
#  provided .bot or .out files.
raw_data.interpolate_ping_bottom_range()

#  Before we transform our power values we need to set the calibration parameters.
#  We typically would load them from an Echoview .ecs file using the
#  load_calibration method but here we're using the fill_default_transceiver_calibration
#  method to set the calibration params. This method pulls the values from data
#  within the .raw file.
raw_data.calibration_params = raw_data.fill_default_transceiver_calibration()

#  now we'll calculate sv
raw_data.Sv(linear=True)

#  and then get a AxisArray object containing the linear SV data. You are highly
#  encouraged to read the method header for raw_reader.to_array()
linear_sv = raw_data.to_array('sv', channel=1)

#  now we will define the integration grid. First we create a GridParameters
#  object which is used to create a grid. The grid cell dimensions cell_height_m
#  interval_length_nmi and are defined above.
int_grid_parms = echolab.grid.GridParameters(layer_spacing=cell_height_m,
                layer_unit='range', interval_spacing=interval_length_nmi,
                interval_unit='distance')

#  next we create a grid by calling the grid method of the GridParameters object.
#  the object returns a grid object based on the data in our linear_sv AxesArray
#  object. We can also plot this grid as you'll see below.
integration_grid = int_grid_parms.grid(linear_sv)

#  The next step is to create the mask which will be used during integration to
#  exclude data from integration. Masks can be combined in various ways to
#  integrate different parts, or regions, of the water column. Currently, you can
#  only create masks based on lines which are defined as either a scalar range
#  or an n pings vector or ranges. You can mask above, below, between, and "not
#  between" lines. You can also create masks based on thresholds (though integration
#  thresholds must be specified during integration and not by masking.)
#
#  In this example we are only going to mask out the data within 10m of the
#  surface (exclude above) and any data 3m above bottom and below (exclude below.)

#  first create the mask object - a mask can be comprised of several "sub masks"
mask = echolab.mask.EchogramMask()

#  Now create the "exclude below" line sub-mask. Since this is surface referenced
#  data, we create the exclude below mask based on the bottom line. As stated,
#  we're integrating to 3m above the bottom so we add 3 to the bottom line when
#  creating the exclude below mask.
#
#  It is important to note here that we treat these "exclude_below" and
#  "exclude_above" masks differently in that during integration the
#  "mean_exclude_below_depth_line" and/or  "mean_exclude_above_depth_line"
#  parameters will be calculated if the exclude_below or exclude_above sub-masks
#  exist in the mask.

#  get the bottom line
bottom_line = (raw_data.get_channel(1)['bottom'])

#  create the exclude below sub-mask based on this bottom line. The line is
#  created 3m above the bottom so we SUBTRACT 3.
mask.mask_below_range(name='exclude_below', reference=bottom_line - 3)

#  create the exclude above sub mask - here we'll exclude all data above 10m (range)
mask.mask_above_range(name='exclude_above', reference=10)

#  finally we can integrate. Here we can specify the min and max thresholds. Since
#  we're settin gthresholds in log units, we need to set the log_thresholds keyword.
int_results, mask_results = echolab.integration.integrate_single_beam_Sv(data=linear_sv,
                            grid=integration_grid, mask=mask, min_threshold=-70,
                            max_threshold=-30, log_thresholds=True)

#  write our results to a couple of files
int_results.to_csv(integration_out_csv)
mask_results.to_csv(mask_out_csv)


#  Create a figure - pyEcholab is pretty thin on the plotting functions right
#  now. The plotting class should be extended to handle plotting grids, masks,
#  and integration results. I'm using this example to work out some of the details
#  of adding this into the plotting class.

#  calculate Sv - we'll plot the echogram as Sv
raw_data.Sv()

#  and then get a AxisArray object containing the SV data.
Sv = raw_data.to_array('Sv', channel=1)

#  create a simple echogram using the echolab plotting package
fig = echoplot.simple_echogram_plot(Sv, grid=integration_grid,
                                    sample_unit='range')


#  now I will plot the exclude masks so we can see what was excluded from
#  integration. This should be rolled into the plotting package once the
#  details are worked out.

#  The basic steps here are to get the mask then create an image using that mask
#  where samples (pixels) that are masked are visible and samples that are not
#  masked are invisible. Then when we draw this image on top of our echogram the
#  samples that are *not* masked show through.

#  first we must get the mask array from the mask object. If we wanted to use
#  different colors for plotting the different sub-masks (in this example the
#  exclude_above and exclude_below masks) we could get the sub masks and then
#  call their mask method. For simplicities sake I'm just plotting the composite
#  mask here. We can pass either Sv or linear_sv to the mask method since they
#  are the same in terms of size and temporal values.
compositeBoolMask = mask.mask(linear_sv)

#  create a (nSamples x nPings, 4) array that we will eventually plot as our mask
#  image. I had trouble directly creating a 4 layer array then assigning to the 4th
#  layer directly since Numpy was returning a copy after slice instead of a view.
#  (In other words, I couldn't modify in place) so I do it in a few steps.

#  first create a 3 channel array that is the same size as our echogram
#  The 3 channels correspond to (R,G,B)
compositeMaskToPlot = np.zeros((Sv.shape[0],Sv.shape[1],3), np.uint8)

#  next, make the whole image a nice purple color
compositeMaskToPlot[:,:,0] = 240
compositeMaskToPlot[:,:,2] = 255

#  the 4th channel in the image is the alpha channel which we will manipulate to
#  only draw over data that is masked. Again, this is the same size as our echogram.
alpha = np.zeros((Sv.shape[0],Sv.shape[1],1), np.uint8)

#  set the value of the alpha channel to 200 where our mask is "true". Samples
#  with "false" mask values will keep their value of 0 which is completely
#  transparent.
alpha[compositeBoolMask] = mask_alpha

#  lastly, we append our alpha channel to the compositeMaskToPlot creating an
#  (R,G,B,A) image that we can plot using imshow
compositeMaskToPlot = np.append(compositeMaskToPlot,alpha,2)

#  we need to set the extents of our mask plot
extents = [compositeBoolMask.axes[1]['ping'][0],
           compositeBoolMask.axes[1]['ping'][-1],
           compositeBoolMask.axes[0]['range'][-1],
           compositeBoolMask.axes[0]['range'][0]]

#  now plot the mask on top of the echogram
fig.gca().imshow(compositeMaskToPlot,extent=extents)

#  Plot the bottom line
#  1/28/17 - Starting around Matplotlib 1.5.x you get an error when directly
#            plotting the bottom_line pd.series object because Matplotlib
#            doesn't handle the datetime64 timestamps correctly. This issue
#            persists thru 2.0.0.
#
#            A workaround is to simply pass the depth values without the index
#            by calling the as_matrix() method.
fig.gca().plot(bottom_line.as_matrix(),'y', linewidth=2)

#  plot the bottom - 3m which will be the beginning of the exclude below mask
fig.gca().plot(bottom_line.as_matrix()-3,'c', linewidth=2)

#  now plot the integration grid - this can be done by simple_echogram_plot
#  but I'm doing it explicitly to have a bit more control over the result.
layer_line_opts = dict(color='gray', alpha=0.8)
ref_line_opts = dict(color='yellow', alpha=1.0, linewidth=2)
integration_grid.plot(axes=fig.gca(), ref_line_opts=ref_line_opts,
                      layer_line_opts=layer_line_opts)


#  finally, show the goods
plt.show(block=True)
