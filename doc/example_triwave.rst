.. currentmodule:: echolab._io.raw_reader

Triangle-wave Correction
========================

Raw data collected with SIMRAD ES60 systems contain an artificial triangle-wave signal superimposed on the raw sample data INSERT REFERENCE HERE.  This artifact can be removed from the data using the member function :member;`RawReader.correct_triangle_wave`


.. automember:: RawReader.correct_triangle-wave

Method
------


Example
-------


