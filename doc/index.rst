.. pyechoLab documentation master file, created by
   sphinx-quickstart on Wed Mar 07 09:19:29 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. currentmodule:: echolab


pyecholab - a python module for SIMRAD raw data
===============================================

pyecholab:
*  Reads and writes SIMRAD raw and associated bottom data

*  References data against arbitrary lines.

*  Easily removes triangle-wave overlays from ES60 systems

*  Checks for and corrects issues arrising from impropperly set aquisition clocks.

*  Grids data based on range, sample, distance, or ping

*  Masks data samples based on range, vaue, or ping 

*  Integrates mean-Sv using above mentioned masks and grids for single-beam transceivers

A quick example, plotting a Sv echogram::

  import echolab
  import echolab.plotting

  raw_filename = r'some/raw/filename.raw'
  bot_filename = r'some/raw/filename.out'

  raw_data = echolab.io_.RawReader([raw_filename, bot_filename])

  raw_data.interpolate_ping_dist()
  raw_data.interpolate_ping_range()

  raw_data.Sv(linear=False)

  Sv_echogram = raw_data.to_array('Sv', channel=1)

  fig = echolab.plotting.simple_echogram_plot(Sv_echogram)

  fig.show()


Contents:

.. toctree::
  :maxdepth: 2

  installing
  conventions
  basic
  calibration
  grid
  mask
  integration
  nmea
  saving
  plotting
  examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

